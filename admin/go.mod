module cb

go 1.16

require (
	github.com/gofiber/fiber/v2 v2.34.0 // indirect
	github.com/gofiber/jwt v0.2.0 // indirect
	github.com/gofiber/jwt/v3 v3.2.12 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/golang-jwt/jwt/v4 v4.4.1 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/klauspost/compress v1.15.6 // indirect
	github.com/rs/cors v1.8.2 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	gorm.io/driver/mysql v1.3.3 // indirect
	gorm.io/gorm v1.23.5 // indirect
)
