package entities

type Tabler3 interface {
	TableName() string
}

func (Ad_brand) TableName() string {
	return "Ad_brand"
}

type Ad_brand struct {
	Bid           int    `gorm:"primary_key" json:"bid"` //
	Oid           int    `json:"oid"`                    //
	Brand_name    string `json:"brand_name"`             //
	Brand_contact string `json:"brand_contact"`          //
	Brand_email   string `json:"brand_email"`            //
	Brand_address string `json:"brand_address"`          //
	Brand_desc    string `json:"brand_desc"`             //
	Cod_brand     string `json:"cod_brand"`
}
