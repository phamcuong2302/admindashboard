package entities

type Tabler2 interface {
	TableName() string
}

func (Ad_organizations) TableName() string {
	return "ad_organization"
}

type Ad_organizations struct {
	Oid                  int    `gorm:"primary_key" json:"oid"` //
	Organization_name    string `json:"organization_name"`      //
	Organization_contact string `json:"organization_contact"`   //
	Organization_email   string `json:"organization_email"`     //
	Organization_address string `json:"organization_address"`   //
	Organization_type    int    `json:"organization_type"`      //
	Organization_desc    string `json:"organization_desc"`      //
	Organization_group   int    `json:"organization_group"`     //
	Cod_organization     string `json:"cod_organization"`       //
}
