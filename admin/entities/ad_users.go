// @/entities/Spersons.go
package entities

type Ad_users struct {
	Uid      int    `gorm:"primary_key" json:"uid"`
	Username string `json:"username"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Role     string `json:"role"`
	Officeid int    `json:"officeid"`
	Level    int    `json:"level"`
	Ip_limit string `json:"ip_limit"`
}
