package entities

import (
	"time"
)

type Tabler1 interface {
	TableName() string
}

func (Query_History) TableName() string {
	return "query_history"
}

type Query_History struct {
	Id                 int       `gorm:"primary_key" json:"id"`                                     //
	Uid                int       `json:"uid"`                                                       //
	Date               time.Time `json:"date" gorm:"type:datetime(6);default:current_timestamp(6)"` //
	Count_query        int       `json:"count_query"`                                               //
	Count_successquery int       `json:"count_successquery"`                                        //
	Count_failquery    int       `json:"count_failquery"`                                           //
}
