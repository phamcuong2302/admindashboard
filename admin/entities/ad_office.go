package entities


type Tabler4 interface {
	TableName() string
}

func (Ad_office) TableName() string {
	return "ad_office"
}
type Ad_office struct {
	Officeid	int	`gorm:"primary_key" json:"officeid"` //
	Bid	int	`json:"bid"` //
	Office_name	string	`json:"office_name"` //
	Office_contact	string	`json:"office_contact"` //
	Office_email	string	`json:"office_email"` //
	Office_address	string	`json:"office_address"` //
	Office_desc	string	`json:"office_desc"` //
}