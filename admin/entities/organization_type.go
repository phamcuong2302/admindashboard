package entities


type Tabler interface {
	TableName() string
}
func (Organization_type) TableName() string {
	return "organization_type"
}
type Organization_type struct {
	Id	int	`gorm:"primary_key" json:"id"` //
	Type_name	string	`json:"type_name"` //
	Type_desc	string	`json:"type_desc"` //
}