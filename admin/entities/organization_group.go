package entities

type Tabler6 interface {
	TableName() string
}

func (Organization_group) TableName() string {
	return "organization_group"
}
type Organization_group struct {
	Id	int	`gorm:"primary_key" json:"id"` //
	Group_name	string	`json:"group_name"` //
	Group_desc	string	`json:"group_desc"` //
}