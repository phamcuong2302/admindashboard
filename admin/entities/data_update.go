package entities

import "time"

type Tabler5 interface {
	TableName() string
}

func (Data_update) TableName() string {
	return "data_update"
}
type Data_update struct {
	Id	int	`gorm:"primary_key" json:"id"` //
	Oid	int	`json:"oid"` //
	Date_send	time.Time	`json:"date_send"` //
	Date_update	time.Time	`json:"date_update"` //
	Status	string	`json:"status"` //
	Count_debit_customers	int	`json:"count_debit_customers"` //
	Count_debit_customers_update	int	`json:"count_debit_customers_update"` //
	Count_loan_contracts	int	`json:"count_loan_contracts"` //
	Count_loan_contracts_update	int	`json:"count_loan_contracts_update"` //
}