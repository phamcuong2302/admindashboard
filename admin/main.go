// @/main.go
package main

import (
	"cb/config"
	router "cb/router/routes.go"
	"log"

	jwtware "github.com/gofiber/jwt/v3"

	"github.com/gofiber/fiber/v2"

	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)


func main() {
	config.ConnectDb()
	app := fiber.New()


	app.Use(logger.New())
	app.Use(logger.New(logger.Config{
	Format: "[${ip}]:${port} ${status} - ${method} ${path} - ${body} ${reqHeaders} \n",
	}))

	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
                AllowOrigins:     "*",
                AllowMethods:     "GET, POST, HEAD, PUT, DELETE, PATCH, OPTIONS",
		AllowHeaders:     "Origin, Content-Type, Accept, Accept-Language, Content-Length,authorization",
	}))

	
	// Login route
	// app.Post("/login", handlers.Login)

	router.SetupRoutes(app)
	// JWT Middleware
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte("secret"),
	}))


       

	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404) // => 404 "Not Found"
	})

	log.Fatal(app.Listen(":5000"))
}
