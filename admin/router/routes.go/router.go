package router

import (
	controller "cb/handlers"

	"github.com/gofiber/fiber/v2"
)

func SetupRoutes(app *fiber.App) {
	api := app.Group("/api")

	app.Post("/login", controller.Login)

	SetupUserRoutes(api)
	SetupBrandRoutes(api)
	SetupOfficeRoutes(api)
	SetupQueryRoutes(api)
	SetupOrganizationQueryRoutes(api)
	SetupDataRoutes(api)
	SetupGroupRoutes(api)
	SetupTypeRoutes(api)
}
func SetupUserRoutes(api fiber.Router) {
	users := api.Group("/user")

	users.Get("/", controller.List)
	users.Put("/", controller.Insert)
	users.Put("/:uid", controller.EditUser)
	users.Delete("/:uid", controller.DeleteUser)
}
func SetupBrandRoutes(api fiber.Router) {
	users := api.Group("/brand")

	users.Get("/", controller.ListBrand)
	users.Put("/", controller.InsertBrand)
	users.Put("/:bid", controller.EditBrand)
	users.Delete("/:bid", controller.DeleteBrand)
}
func SetupOfficeRoutes(api fiber.Router) {
	users := api.Group("/office")

	users.Get("/", controller.ListOffice)
	users.Put("/", controller.InsertOffice)
	users.Put("/:officeid", controller.EditOffice)
	users.Delete("/:officeid", controller.DeleteOffice)
}

func SetupQueryRoutes(api fiber.Router) {
	users := api.Group("/query")

	users.Get("/", controller.ListQuery)
	users.Put("/", controller.InsertQuery)
	users.Put("/:id", controller.EditQuery)
	users.Delete("/:id", controller.DeleteQuery)
}

func SetupOrganizationQueryRoutes(api fiber.Router) {
	users := api.Group("/organization")

	users.Get("/", controller.ListOrganization)
	users.Get("/:oid", controller.GetOrganization)
	users.Put("/", controller.InsertOrganization)
	users.Put("/:oid", controller.EditOrganization)
	users.Delete("/:oid", controller.DeleteOrganization)
}
func SetupDataRoutes(api fiber.Router) {
	users := api.Group("/data")

	users.Get("/", controller.ListData)
	users.Put("/", controller.InsertData)
	users.Put("/:id", controller.EditData)
	users.Delete("/:id", controller.DeleteData)
}

func SetupTypeRoutes(api fiber.Router) {
	users := api.Group("/type")

	users.Get("/", controller.ListType)
	users.Put("/", controller.InsertType)
	users.Put("/:id", controller.EditType)
	users.Delete("/:id", controller.DeleteType)
}
func SetupGroupRoutes(api fiber.Router) {
	users := api.Group("/group")

	users.Get("/", controller.ListGroup)
	users.Put("/", controller.InsertGroup)
	users.Put("/:id", controller.EditGroup)
	users.Delete("/:id", controller.DeleteGroup)
}
