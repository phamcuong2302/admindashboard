package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListType(c *fiber.Ctx) error {

	var list []entities.Organization_type
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func InsertType(ctx *fiber.Ctx) error {

	typeorg := new(entities.Organization_type)
	if err := ctx.BodyParser(typeorg); err != nil {
		panic("An error occurred when parsing the new brand: " + err.Error())
	}

	if response := config.DBConn.Create(&typeorg); response.Error != nil {
		panic("An error occurred when storing the new brand: " + response.Error.Error())
	}
	err := ctx.JSON(typeorg)
	if err != nil {
		panic("Error occurred when returning JSON of a brand: " + err.Error())
	}
	return err
}

func EditType(ctx *fiber.Ctx) error {
	id := ctx.Params("bid")
	editType := new(entities.Organization_type)
	typeorg := new(entities.Organization_type)
	if err := ctx.BodyParser(editType); err != nil {
		panic("An error occurred when parsing the edited type: " + err.Error())
	}
	if response := config.DBConn.Find(&typeorg, id); response.Error != nil {
		panic("An error occurred when retrieving the existing type: " + response.Error.Error())
	}
	// User does not exist
	if typeorg.Id == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"ID": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a type: " + err.Error())
		}
		return err
	}
	typeorg.Type_name = editType.Type_name
	typeorg.Type_desc = editType.Type_desc

	// Save user
	config.DBConn.Save(&typeorg)

	err := ctx.JSON(typeorg)
	if err != nil {
		panic("Error occurred when returning JSON of a type: " + err.Error())
	}
	return err
}

func DeleteType(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	var typeorg entities.Organization_type
	config.DBConn.Find(&typeorg, id)
	if response := config.DBConn.Find(&typeorg); response.Error != nil {
		panic("An error occurred when finding the type to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&typeorg)

	err := ctx.JSON(fiber.Map{
		"id":     id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a type: " + err.Error())
	}
	return err
}
