package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListData(c *fiber.Ctx) error {

	var list []entities.Data_update
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func InsertData(ctx *fiber.Ctx) error {

	data := new(entities.Data_update)
	if err := ctx.BodyParser(data); err != nil {
		panic("An error occurred when parsing the new data: " + err.Error())
	}

	if response := config.DBConn.Create(&data); response.Error != nil {
		panic("An error occurred when storing the new data: " + response.Error.Error())
	}
	err := ctx.JSON(data)
	if err != nil {
		panic("Error occurred when returning JSON of a data: " + err.Error())
	}
	return err
}

func EditData(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	editData := new(entities.Data_update)
	data := new(entities.Data_update)
	if err := ctx.BodyParser(editData); err != nil {
		panic("An error occurred when parsing the edited data: " + err.Error())
	}
	if response := config.DBConn.Find(&data, id); response.Error != nil {
		panic("An error occurred when retrieving the existing data: " + response.Error.Error())
	}
	// User does not exist
	if data.Id == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"ID": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a data: " + err.Error())
		}
		return err
	}
	data.Date_send = editData.Date_send
	data.Date_update = editData.Date_update
	data.Status = editData.Status
	data.Count_debit_customers = editData.Count_debit_customers
	data.Count_debit_customers_update = editData.Count_debit_customers_update
	data.Count_loan_contracts = editData.Count_loan_contracts
	data.Count_loan_contracts_update = editData.Count_loan_contracts_update
	// Save user
	config.DBConn.Save(&data)

	err := ctx.JSON(data)
	if err != nil {
		panic("Error occurred when returning JSON of a data: " + err.Error())
	}
	return err
}

func DeleteData(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	var data entities.Data_update
	config.DBConn.Find(&data, id)
	if response := config.DBConn.Find(&data); response.Error != nil {
		panic("An error occurred when finding the data to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&data)

	err := ctx.JSON(fiber.Map{
		"Id":     id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a data: " + err.Error())
	}
	return err
}
