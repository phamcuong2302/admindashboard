package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func List(c *fiber.Ctx) error {

	list := []entities.Ad_users{}
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func Insert(ctx *fiber.Ctx) error {

	user := new(entities.Ad_users)
	if err := ctx.BodyParser(user); err != nil {
		panic("An error occurred when parsing the new user: " + err.Error())
	}

	if response := config.DBConn.Create(&user); response.Error != nil {
		panic("An error occurred when storing the new user: " + response.Error.Error())
	}
	err := ctx.JSON(user)
	if err != nil {
		panic("Error occurred when returning JSON of a user: " + err.Error())
	}
	return err
}

func EditUser(ctx *fiber.Ctx) error {
	id := ctx.Params("uid")
	editUser := new(entities.Ad_users)
	user := new(entities.Ad_users)
	if err := ctx.BodyParser(editUser); err != nil {
		panic("An error occurred when parsing the edited user: " + err.Error())
	}
	if response := config.DBConn.Find(&user, id); response.Error != nil {
		panic("An error occurred when retrieving the existing user: " + response.Error.Error())
	}
	// User does not exist
	if user.Uid == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"ID": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a user: " + err.Error())
		}
		return err
	}
	user.Username = editUser.Username
	user.Password = editUser.Password
	user.Name = editUser.Name
	user.Role = editUser.Role
	user.Officeid = editUser.Officeid
	// Save user
	config.DBConn.Save(&user)

	err := ctx.JSON(user)
	if err != nil {
		panic("Error occurred when returning JSON of a user: " + err.Error())
	}
	return err
}

func DeleteUser(ctx *fiber.Ctx) error {
	id := ctx.Params("uid")
	var user entities.Ad_users
	config.DBConn.Find(&user, id)
	if response := config.DBConn.Find(&user); response.Error != nil {
		panic("An error occurred when finding the user to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(user)

	err := ctx.JSON(fiber.Map{
		"Uid":     id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a user: " + err.Error())
	}
	return err
}
