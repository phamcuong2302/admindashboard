package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListOrganization(c *fiber.Ctx) error {

	var list []entities.Ad_organizations
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}
func GetOrganization(ctx *fiber.Ctx) error {
	var org entities.Ad_organizations
	id := ctx.Params("oid")
	if response := config.DBConn.Find(&org, id); response.Error != nil {
		panic("Error occurred while retrieving organization from the database: " + response.Error.Error())
	}
	err := ctx.JSON(org)
	if org.Oid == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}

		if err != nil {
			panic("Error occurred when returning JSON of a organization: " + err.Error())
		}
		return err
	}
	return err

}


func InsertOrganization(ctx *fiber.Ctx) error {

	organization := new(entities.Ad_organizations)
	if err := ctx.BodyParser(organization); err != nil {
		panic("An error occurred when parsing the new organization: " + err.Error())
	}

	if response := config.DBConn.Create(&organization); response.Error != nil {
		panic("An error occurred when storing the new organization: " + response.Error.Error())
	}
	err := ctx.JSON(organization)
	if err != nil {
		panic("Error occurred when returning JSON of a organization: " + err.Error())
	}
	return err
}

func EditOrganization(ctx *fiber.Ctx) error {
	id := ctx.Params("oid")
	editOrg := new(entities.Ad_organizations)
	organization := new(entities.Ad_organizations)
	if err := ctx.BodyParser(editOrg); err != nil {
		panic("An error occurred when parsing the edited brand: " + err.Error())
	}
	if response := config.DBConn.Find(&organization, id); response.Error != nil {
		panic("An error occurred when retrieving the existing brand: " + response.Error.Error())
	}
	// User does not exist
	if organization.Oid == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"ID": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a brand: " + err.Error())
		}
		return err
	}
	organization.Organization_name = editOrg.Organization_name
	organization.Organization_contact = editOrg.Organization_contact
	organization.Organization_email = editOrg.Organization_email
	organization.Organization_address = editOrg.Organization_address
	organization.Organization_type = editOrg.Organization_type
	organization.Organization_desc = editOrg.Organization_desc
	organization.Organization_group = editOrg.Organization_group
	organization.Cod_organization = editOrg.Cod_organization
	// Save user
	config.DBConn.Save(&organization)

	err := ctx.JSON(organization)
	if err != nil {
		panic("Error occurred when returning JSON of a brand: " + err.Error())
	}
	return err
}

func DeleteOrganization(ctx *fiber.Ctx) error {
	id := ctx.Params("oid")
	var organization entities.Ad_organizations
	config.DBConn.Find(&organization, id)
	if response := config.DBConn.Find(&organization); response.Error != nil {
		panic("An error occurred when finding the organization to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&organization)

	err := ctx.JSON(fiber.Map{
		"Oid":     id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a organization: " + err.Error())
	}
	return err
}
