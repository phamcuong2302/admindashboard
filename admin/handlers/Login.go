// @/handlers/subject_info.go
package handlers

import (
	"cb/config"
	"cb/entities"
	"crypto/md5"
	"encoding/hex"
	"time"

	"github.com/gofiber/fiber/v2"

	"github.com/golang-jwt/jwt/v4"
)

func Login(c *fiber.Ctx) error {
	username := c.FormValue("username")
	password := c.FormValue("password")
	users := []entities.Ad_users{}
	config.DBConn.Where("username = ? AND password = ? AND role = 'admin' AND level = 0", username, GetMD5Hash(password)).Limit(1).Find(&users)

	// Throws Unauthorized error
	if len(users) == 0 {
		return c.SendStatus(fiber.StatusUnauthorized)
	} else {
		// Create the Claims
		claims := jwt.MapClaims{
			"name":  users[0].Name,
			"admin": true,
			"exp":   time.Now().Add(time.Hour * 6).Unix(),
		}

		// Create token
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return c.SendStatus(fiber.StatusInternalServerError)
		}

		return c.JSON(fiber.Map{"token": t})

	}

}

func GetMD5Hash(text string) string {
	hash := md5.Sum([]byte(text))
	return hex.EncodeToString(hash[:])
}
