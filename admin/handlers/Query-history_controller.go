package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListQuery(c *fiber.Ctx) error {

	var list []entities.Query_History
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func InsertQuery(ctx *fiber.Ctx) error {

	query := new(entities.Query_History)
	if err := ctx.BodyParser(query); err != nil {
		panic("An error occurred when parsing the new query: " + err.Error())
	}

	if response := config.DBConn.Create(&query); response.Error != nil {
		panic("An error occurred when storing the new query: " + response.Error.Error())
	}
	err := ctx.JSON(query)
	if err != nil {
		panic("Error occurred when returning JSON of a query: " + err.Error())
	}
	return err
}

func EditQuery(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	editQuery := new(entities.Query_History)
	query := new(entities.Query_History)
	if err := ctx.BodyParser(editQuery); err != nil {
		panic("An error occurred when parsing the edited query: " + err.Error())
	}
	if response := config.DBConn.Find(&query, id); response.Error != nil {
		panic("An error occurred when retrieving the existing query: " + response.Error.Error())
	}
	// User does not exist
	if query.Id == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"Id": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a query: " + err.Error())
		}
		return err
	}
	query.Uid = editQuery.Uid
	query.Date = editQuery.Date
	query.Count_query = editQuery.Count_query
	query.Count_successquery = editQuery.Count_successquery
	query.Count_failquery = editQuery.Count_failquery

	// Save user
	config.DBConn.Save(&query)

	err := ctx.JSON(query)
	if err != nil {
		panic("Error occurred when returning JSON of a query: " + err.Error())
	}
	return err
}

func DeleteQuery(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	var query entities.Query_History
	config.DBConn.Find(&query, id)
	if response := config.DBConn.Find(&query); response.Error != nil {
		panic("An error occurred when finding the query to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&query)

	err := ctx.JSON(fiber.Map{
		"Id":      id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a query: " + err.Error())
	}
	return err
}
