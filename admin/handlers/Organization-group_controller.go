package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListGroup(c *fiber.Ctx) error {

	var list []entities.Organization_group
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func InsertGroup(ctx *fiber.Ctx) error {

	grouporg := new(entities.Organization_group)
	if err := ctx.BodyParser(grouporg); err != nil {
		panic("An error occurred when parsing the new grouporg: " + err.Error())
	}

	if response := config.DBConn.Create(&grouporg); response.Error != nil {
		panic("An error occurred when storing the new grouporg: " + response.Error.Error())
	}
	err := ctx.JSON(grouporg)
	if err != nil {
		panic("Error occurred when returning JSON of a grouporg: " + err.Error())
	}
	return err
}

func EditGroup(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	editGroup := new(entities.Organization_group)
	grouporg := new(entities.Organization_group)
	if err := ctx.BodyParser(editGroup); err != nil {
		panic("An error occurred when parsing the edited type: " + err.Error())
	}
	if response := config.DBConn.Find(&grouporg, id); response.Error != nil {
		panic("An error occurred when retrieving the existing type: " + response.Error.Error())
	}
	// User does not exist
	if grouporg.Id == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"ID": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a group: " + err.Error())
		}
		return err
	}
	grouporg.Group_name = editGroup.Group_name
	grouporg.Group_desc = editGroup.Group_desc

	// Save user
	config.DBConn.Save(&grouporg)

	err := ctx.JSON(grouporg)
	if err != nil {
		panic("Error occurred when returning JSON of a group: " + err.Error())
	}
	return err
}

func DeleteGroup(ctx *fiber.Ctx) error {
	id := ctx.Params("id")
	var grouporg entities.Organization_group
	config.DBConn.Find(&grouporg, id)
	if response := config.DBConn.Find(&grouporg); response.Error != nil {
		panic("An error occurred when finding the group to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&grouporg)

	err := ctx.JSON(fiber.Map{
		"id":      id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a group: " + err.Error())
	}
	return err
}
