package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListBrand(c *fiber.Ctx) error {

	var list []entities.Ad_brand
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func InsertBrand(ctx *fiber.Ctx) error {

	brand := new(entities.Ad_brand)
	if err := ctx.BodyParser(brand); err != nil {
		panic("An error occurred when parsing the new brand: " + err.Error())
	}

	if response := config.DBConn.Create(&brand); response.Error != nil {
		panic("An error occurred when storing the new brand: " + response.Error.Error())
	}
	err := ctx.JSON(brand)
	if err != nil {
		panic("Error occurred when returning JSON of a brand: " + err.Error())
	}
	return err
}

func EditBrand(ctx *fiber.Ctx) error {
	id := ctx.Params("bid")
	editBrand := new(entities.Ad_brand)
	brand := new(entities.Ad_brand)
	if err := ctx.BodyParser(editBrand); err != nil {
		panic("An error occurred when parsing the edited brand: " + err.Error())
	}
	if response := config.DBConn.Find(&brand, id); response.Error != nil {
		panic("An error occurred when retrieving the existing brand: " + response.Error.Error())
	}
	// User does not exist
	if brand.Bid == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"ID": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a brand: " + err.Error())
		}
		return err
	}
	brand.Brand_name = editBrand.Brand_name
	brand.Brand_contact = editBrand.Brand_contact
	brand.Brand_email = editBrand.Brand_email
	brand.Brand_address = editBrand.Brand_address
	brand.Brand_desc = editBrand.Brand_desc
	brand.Cod_brand = editBrand.Cod_brand
	// Save user
	config.DBConn.Save(&brand)

	err := ctx.JSON(brand)
	if err != nil {
		panic("Error occurred when returning JSON of a brand: " + err.Error())
	}
	return err
}

func DeleteBrand(ctx *fiber.Ctx) error {
	id := ctx.Params("bid")
	var brand entities.Ad_brand
	config.DBConn.Find(&brand, id)
	if response := config.DBConn.Find(&brand); response.Error != nil {
		panic("An error occurred when finding the brand to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&brand)

	err := ctx.JSON(fiber.Map{
		"Bid":     id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a brand: " + err.Error())
	}
	return err
}
