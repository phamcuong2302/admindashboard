package handlers

import (
	"cb/config"
	"cb/entities"

	"github.com/gofiber/fiber/v2"
)

//Get User List
func ListOffice(c *fiber.Ctx) error {

	var list []entities.Ad_office
	config.DBConn.Find(&list)

	return c.Status(200).JSON(list)
}

func InsertOffice(ctx *fiber.Ctx) error {

	office := new(entities.Ad_office)
	if err := ctx.BodyParser(office); err != nil {
		panic("An error occurred when parsing the new office: " + err.Error())
	}

	if response := config.DBConn.Create(&office); response.Error != nil {
		panic("An error occurred when storing the new office: " + response.Error.Error())
	}
	err := ctx.JSON(office)
	if err != nil {
		panic("Error occurred when returning JSON of a office: " + err.Error())
	}
	return err
}

func EditOffice(ctx *fiber.Ctx) error {
	id := ctx.Params("officeid")
	editOffice := new(entities.Ad_office)
	office := new(entities.Ad_office)
	if err := ctx.BodyParser(editOffice); err != nil {
		panic("An error occurred when parsing the edited office: " + err.Error())
	}
	if response := config.DBConn.Find(&office, id); response.Error != nil {
		panic("An error occurred when retrieving the existing office: " + response.Error.Error())
	}
	// User does not exist
	if office.Officeid == 0 {
		err := ctx.SendStatus(fiber.StatusNotFound)
		if err != nil {
			panic("Cannot return status not found: " + err.Error())
		}
		err = ctx.JSON(fiber.Map{
			"Officeid": id,
		})
		if err != nil {
			panic("Error occurred when returning JSON of a office: " + err.Error())
		}
		return err
	}
	office.Bid = editOffice.Bid
	office.Office_name = editOffice.Office_name
	office.Office_contact = editOffice.Office_contact
	office.Office_email = editOffice.Office_email
	office.Office_address = editOffice.Office_address
	office.Office_desc = editOffice.Office_desc
	// Save user
	config.DBConn.Save(&office)

	err := ctx.JSON(office)
	if err != nil {
		panic("Error occurred when returning JSON of a office: " + err.Error())
	}
	return err
}

func DeleteOffice(ctx *fiber.Ctx) error {
	id := ctx.Params("officeid")
	var office entities.Ad_office
	config.DBConn.Find(&office, id)
	if response := config.DBConn.Find(&office); response.Error != nil {
		panic("An error occurred when finding the brand to be deleted" + response.Error.Error())
	}
	config.DBConn.Delete(&office)

	err := ctx.JSON(fiber.Map{
		"id":     id,
		"Deleted": true,
	})
	if err != nil {
		panic("Error occurred when returning JSON of a office: " + err.Error())
	}
	return err
}
